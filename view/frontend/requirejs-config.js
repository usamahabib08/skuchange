

var config = {
    config: {
        mixins: {
            'Magento_ConfigurableProduct/js/configurable': {
                'Sanipex_SkuChange/js/model/skuswitch': true
            },
            'Magento_Swatches/js/swatch-renderer': {
                'Sanipex_SkuChange/js/model/swatch-skuswitch': true
            }
        }
    }
};